<?php

function paolayorestes_setup() {
    load_theme_textdomain( 'paolayorestes', get_template_directory() . '/languages' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );
}
add_action( 'after_setup_theme', 'paolayorestes_setup' );

function paolayorestes_scripts() {
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Poppins:300&display=swap' );
    wp_enqueue_style( 'paolayorestes-style', get_stylesheet_uri(), '', time() );

    wp_enqueue_script( 'paolayorestes-js', get_template_directory_uri() . '/assets/js/app.js', array('jquery'), time(), true );
}
add_action('wp_enqueue_scripts', 'paolayorestes_scripts');

// GravityForms compatibility file
require_once get_template_directory() . '/inc/pyo-gravityforms.php';