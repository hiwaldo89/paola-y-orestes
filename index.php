<?php get_header(); ?>
    <div class="welcome-section d-flex align-items-center justify-content-center position-relative">
        <div id="welcome-section" class="parallax-scene">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/bg-triangles.svg" alt="triangles" class="triangles" data-depth="0.6">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/icon.png" alt="Paola y Orestes" class="icon" data-depth="0.2">
        </div>
    </div>

    <div class="container py-5 text-center site-title position-relative">
        <h1 class="h3 text-white text-uppercase">Paola & Orestes</h1>
        <img src="<?php bloginfo('template_url'); ?>/assets/img/name-shadow.svg" alt="">
    </div>

    <div class="container text-center position-relative pt-5 mt-5">
        <a href="#" data-toggle="modal" data-target="#video-modal" class="d-inline-block play-btn">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/play.svg" alt="Ver video" width="80" class="mt-4">
        </a>
    </div>

    <div class="schedule py-5">
        <div class="shooting-stars parallax-item" data-paroller-factor="0.8" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/shooting-stars-1.svg" alt="Shooting starts" class="img-fluid">
        </div>
        <div class="container">
            <div class="py-5 my-5 text-center">
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">18 de octubre</span>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-white mb-4 letter-spacing">
                            <span class="h4">7:00pm</h1>
                        </div>
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">Callejoneada</span>
                        </div>
                        <div class="text-white px-md-5 schedule__info text-left mt-4 mt-md-0">
                            <a href="https://www.google.com.mx/maps/place/Plaza+de+Armas+Quer%C3%A9taro/@20.592996,-100.3918293,17z/data=!3m1!4b1!4m5!3m4!1s0x85d35b2b8fba8ac9:0xa5e75226f38fe9af!8m2!3d20.592996!4d-100.3896406" target="_blank">
                                Plaza de Armas <br>
                                Col. Centro <br>
                                Querétaro, Qro.
                                <div>
                                    <span>Ver en Google Maps</span>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow.svg" alt="flecha" class="arrow d-block">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">19 de octubre</span>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-white mb-4 letter-spacing">
                            <span class="h4">3:30pm</h1>
                        </div>
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">Ceremonia</span>
                        </div>
                        <div class="text-white px-md-5 schedule__info text-left mt-4 mt-md-0">
                            <a href="https://www.google.com.mx/maps/place/Templo+De+San+Antonio+De+Padua/@20.5946529,-100.3939196,17z/data=!3m1!4b1!4m5!3m4!1s0x85d35b2bd3750e2d:0xbe55535775fc450!8m2!3d20.5946529!4d-100.3917309" target="_blank">
                                San Antonio de Padua <br>
                                Angela Peralta 16 <br>
                                Col. Centro <br>
                                Querétaro, Qro.
                                <div>
                                    <span>Ver en Google Maps</span>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow.svg" alt="flecha" class="arrow d-block">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-white mb-4 letter-spacing">
                            <span class="h4">6:00pm</h1>
                        </div>
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">Recepción</span>
                        </div>
                        <div class="text-white px-md-5 schedule__info text-left text-md-right mt-4 mt-md-0">
                            <a href="https://www.google.com.mx/maps/place/Hacienda+Tlacote+el+Bajo/@20.6612528,-100.5056447,17z/data=!3m1!4b1!4m5!3m4!1s0x85d351fd0c3655bb:0xd25b9520ad6adc31!8m2!3d20.6612528!4d-100.503456" target="_blank">
                                Hacienda Tlacote El Bajo <br>
                                Miguel Hidalgo 345A <br>
                                Col. Centro <br>
                                Tlacote El Bajo, Qro.
                                <div>
                                    <span>Ver en Google Maps</span>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/img/arrow.svg" alt="flecha" class="arrow d-block">
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-white mb-4 letter-spacing">
                            <span class="h4">Formal</h1>
                        </div>
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">Dresscode</span>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-white mb-4 letter-spacing">
                            <span class="h4">Favor de confirmar asistencia</h1>
                        </div>
                        <button type="button" class="pyo-btn" data-toggle="modal" data-target="#confirmation-form">Confirmar asistencia</button>
                        <!-- <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">R.S.V.P.</span>
                        </div> -->
                    </div>
                </div>
            </div> 
        </div>
    </div>

    <div class="gallery-slider">
        <?php $gallery = get_field('galeria'); ?>
        <?php foreach($gallery as $key=>$slide) : ?>
            <div class="px-lg-3 letter-spacing gallery-slider__item">
                <div class="position-relative gallery-slider__img">
                    <a href="<?php echo $slide['url']; ?>" class="chocolat-image">
                        <img src="<?php echo $slide['url']; ?>" alt="" class="d-block img-fluid">
                    </a>
                </div>
                <div class="text-green h6 mt-3"><?php printf("%02d", $key + 1); ?></div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="schedule py-5">
        <div class="shooting-stars-2 parallax-item" data-paroller-factor="0.8" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/shooting-stars-2.svg" alt="" class="img-fluid">
        </div>
        <div class="shooting-stars-3 parallax-item" data-paroller-factor="0.3" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/shooting-stars-3.svg" alt="" class="img-fluid">
        </div>
        <div class="shooting-stars-4 parallax-item" data-paroller-factor="0.2" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/shooting-stars-4.svg" alt="" class="img-fluid">
        </div>
        <div class="shooting-stars-5 parallax-item" data-paroller-factor="1.3" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/shooting-stars-5.svg" alt="" class="img-fluid">
        </div>
        <div class="shooting-stars-6 parallax-item" data-paroller-factor="0.3" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/shooting-stars-6.svg" alt="" class="img-fluid">
        </div>
        <div class="shooting-stars-7 parallax-item" data-paroller-factor="0.6" data-paroller-type="foreground" data-paroller-direction="horizontal">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/shooting-stars-7.svg" alt="" class="img-fluid">
        </div>
        <div class="container">
            <div class="py-5 my-5 text-center">
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <a href="https://mesaderegalos.liverpool.com.mx/milistaderegalos/50167077" target="_blank">
                            <div class="text-white mb-4 letter-spacing">
                                <span class="h4">Liverpool 50167077</h1>
                            </div>
                            <div class="text-green text-uppercase letter-spacing">
                                <span class="h5">Mesa de regalos</span>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">Hospedaje sugerido</span>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <a href="https://www.ihg.com/holidayinnexpress/hotels/us/en/queretaro/mqerg/hoteldetail?cm_mmc=GoogleMaps-_-EX-_-MX-_-MQERG" target="_blank">
                            <div class="text-white mb-4 letter-spacing">
                                <span class="h4">Holiday Inn Express</h1>
                            </div>
                        </a>
                        <a href="tel:018004207000">
                            <div class="text-white-50 text-uppercase letter-spacing mb-4">
                                <span class="h4"><small>Tel: 01 800 420 7000</small></span>
                            </div>
                        </a>
                        <div class="text-white-50 mb-4 letter-spacing">
                            <span class="h4"><small>Código de reservación:</small></h1>
                        </div>
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5"><small>PYO</small></span>
                        </div>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <a href="https://www.marriott.com/hotels/travel/qropt-fairfield-inn-and-suites-queretaro-juriquilla/?scid=bb1a189a-fec3-4d19-a255-54ba596febe2" target="_blank">
                            <div class="text-white mb-4 letter-spacing">
                                <span class="h4">Fairfield by Marriott</h1>
                            </div>
                        </a>
                        <a href="tel:4425009008">
                            <div class="text-white-50 text-uppercase letter-spacing mb-4">
                                <span class="h4"><small>Tel: 442 500 9008</small></span>
                            </div>
                        </a>
                        <div class="text-white-50 mb-4 letter-spacing">
                            <span class="h4"><small>Código de reservación:</small></h1>
                        </div>
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5"><small>boda Paola Núñez y Orestes Ortega</small></span>
                        </div>
                        </a>
                    </div>
                </div>
                <div class="schedule-block py-5">
                    <div class="px-5 my-5 d-inline-block position-relative">
                        <div class="text-green text-uppercase letter-spacing">
                            <span class="h5">Menú de belleza</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row beauty-list">
                <div class="triangles parallax-scene">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/bg-triangle.svg" alt="" class="bg-triangle" data-depth="0.6">
                    <img src="<?php bloginfo('template_url'); ?>/assets/img/triangle-1.svg" alt="" class="bg-triangle" data-depth="0.3">
                </div>
                <div class="col-lg-8 mx-lg-auto text-center">
                    <ul class="nav nav-tabs d-flex justify-content-center mb-5" id="beauty-services" role="tablist">
                        <li class="nav-item mb-5 mb-lg-0">
                            <a class="nav-link active text-white" id="places-tab" data-toggle="tab" href="#places" role="tab" aria-controls="places" aria-selected="true">
                                Lugares
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" id="homeservice-tab" data-toggle="tab" href="#homeservice" role="tab" aria-controls="homeservice" aria-selected="false">
                                Servicio a domicilio
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content d-inline-block text-left" id="">
                        <div class="tab-pane fade show active text-white" id="places" role="tabpanel" aria-labelledby="places-tab">
                            <?php if(have_rows('lugares')) : while(have_rows('lugares')) :  the_row(); ?>
                                <div class="text-white py-4 text-center text-md-left">
                                    <?php the_sub_field('contenido'); ?>
                                </div>
                            <?php endwhile; endif; ?>
                        </div>
                        <div class="tab-pane fade text-white text-center text-md-left" id="homeservice" role="tabpanel" aria-labelledby="homeservice-tab text-white">
                        <?php if(have_rows('servicio_a_domicilio')) : while(have_rows('servicio_a_domicilio')) :  the_row(); ?>
                                <div class="text-white py-4">
                                    <?php the_sub_field('contenido'); ?>
                                </div>
                            <?php endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="py-5 position-relative pyo-footer">
        <img src="<?php bloginfo('template_url'); ?>/assets/img/footer-circles.svg" alt="" class="footer-circles">
        <div class="container text-center py-5">
            <h5 class="text-center text-green text-uppercase letter-spacing h4">Paola y Orestes</h5>
            <p class="text-center text-gray letter-spacing h5">19 octubre 2019</p>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/assets/img/footer-stars.png" alt="" class="footer-stars">
    </footer>
<?php get_footer(); ?>