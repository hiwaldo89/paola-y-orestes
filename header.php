<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <main class="position-relative">
        <div class="stars" style="background:#000 url(<?php bloginfo('template_url'); ?>/assets/img/stars.png) repeat top center; z-index: 0;"></div>
        <div class="twinkling" style="background:transparent url(<?php bloginfo('template_url'); ?>/assets/img/twinkling.png) repeat top center;"></div>
        <!-- <div class="star-bg" style="background: url(<?php bloginfo('template_url'); ?>/assets/img/star-bg.png) repeat center; background-size: 100% auto;" data-paroller-factor="-0.5" data-paroller-factor-sm="-0.2" data-paroller-factor-xs="-0.1"></div> -->