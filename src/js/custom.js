import $ from "jquery";
import Parallax from "parallax-js";
import "chocolat";
import Chocolat from "chocolat";

$(".parallax-scene").each(function() {
  var scene = $(this).get(0);
  new Parallax(scene);
});
// var scene = document.getElementById("welcome-section");
// var parallaxInstance = new Parallax(scene);

$(".gallery-slider").slick({
  centerMode: true,
  slidesToShow: 1,
  centerPadding: "35%",
  arrows: false,
  autoplay: true,
  autoplaySpeed: 1500,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        centerPadding: "15px"
      }
    }
  ]
});

$(".star-bg").paroller();
$(".parallax-item").paroller();

$.fn.Chocolat = Chocolat;

$(".gallery-slider").Chocolat();
